# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 22:54:57 2022

@author: jain
"""

import numpy as np
import matplotlib.pyplot as plt
import cv2
# go one direction till hit obs, repeat
# record whereever u have been 
# if can't go anywhere, back track, each node check if there is an open direction
# go in that direction and repeat until

############################################################################
# Defining Workspace
def Maze(length, breadth, obstacles):
    # fn takes dimensions for maze
    # obstacles is a nested list [centre,diameter]
    # returns workspace - numpy matrix with 0s and 1s as elements
    maze = np.zeros((length+2,breadth+2))
    
    # making obstacles as 1s
    for i in range(length+2):
        for j in range(breadth+2):
            if ((j-5)**2+(i-(5))**2 <= 2**2): # circle
                maze[i][j] = -1

    # making borders to be as obstacles
    maze[0][:] = -1
    maze[11][:] = -1
    maze[:,11] = -1
    maze[:,0] = -1
    return maze

# maze = Maze(10,10,1)
############################################################################

# Define Robot

# # initial position of robot
# p = [10,1]

def check_N(p):
    p_x, p_y = p[0], p[1]
    if maze[p_x-1][p_y] != -1:
        # p_mod = [p_x-1,p_y]
        return True
    else: return False 
    
def check_S(p):
    p_x, p_y = p[0], p[1]
    if maze[p_x+1][p_y] != -1:
        # p_mod = [p_x-1,p_y]
        return True
    else: return False 

def check_E(p):
    p_x, p_y = p[0], p[1]
    if maze[p_x][p_y+1] != -1:
        # p_mod = [p_x-1,p_y]
        return True
    else: return False 
    
def check_W(p):
    p_x, p_y = p[0], p[1]
    if maze[p_x][p_y-1] != -1:
        # p_mod = [p_x-1,p_y]
        return True
    else: return False 

def go_N(p):
    p_x, p_y = p[0], p[1]
    p_mod = [p_x-1,p_y]
    return p_mod

def go_E(p):
    p_x, p_y = p[0], p[1]
    p_mod = [p_x,p_y+1]
    return p_mod

def go_W(p):
    p_x, p_y = p[0], p[1]
    p_mod = [p_x,p_y-1]
    return p_mod

def go_S(p):
    p_x, p_y = p[0], p[1]
    p_mod = [p_x+1,p_y]
    return p_mod
    
def stuck(curr_p):
    # if there is no 0 around
    if not check_N(curr_p) and not check_S(curr_p) and not check_E(curr_p) and not check_W(curr_p):
        return True
    else: return False

# waypoints = []
frame = []
def move(p,maze):
    while not stuck(p):
        if check_N(p):
            p = go_N(p)
            maze[p[0]][p[1]] = -1
        elif check_E(p):
            p = go_E(p)
            maze[p[0]][p[1]] = -1
        elif check_S(p):
            p = go_S(p)
            maze[p[0]][p[1]] = -1
        elif check_W(p):
            p = go_W(p)
            maze[p[0]][p[1]] = -1
        
        waypoints.append(p)
        frame.append(maze.copy())
        # plt.imshow(maze),plt.show()
    return p,maze
    
# p,maze = move(p,maze)

# do backtracking as soon as you are stuck
# take p as last entry of waypoint 
# check for surrounding nodes 
# if they are all blocked, append p and take second last node
# repeat check
# if open path found go all the way there till not stuck

def backtrack():
    for p in waypoints[::-1]:
        if stuck(p):
            waypoints.append(p)
            continue
        else: 
            waypoints.append(p)
            return True
            # p,maze = move(p,maze)
    return False

###########################################################
# main
maze = Maze(10,10,1)
# initial position of robot
p = [10,1]
waypoints = []

p, maze = move(p, maze)
print(len(waypoints))
while backtrack():

    p, maze = move(p, maze)
    print(len(waypoints))
    backtrack()
    p = waypoints[-1]
    print(len(waypoints))
############################################################

###################################################################################
class Node:
    def __init__(self, position, mn_dist, eu_dist):
        self.position = position
        self.mn_dist = mn_dist
        self.eu_dist = eu_dist


start = [10,0]
tracker = []
w = list(set(tuple(sub) for sub in waypoints))
for e in w:
    position = e
    mn_dist = abs(start[0]-e[0])+abs(start[1]-e[1])
    eu_dist = ((start[0]-e[0])**2+(start[1]-e[1])**2)**(1/2)
    tracker.append(Node(position,mn_dist,eu_dist))
    
    
    
    
