# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 15:45:06 2022

@author: jain
"""

import numpy as np
import matplotlib.pyplot as plt

# task - comlpete exploration of workspace by point robot
# Print out the positions and diameters of the obstacles chosen and
# also the coordinates of the waypoints selected by the program.

# Plan - lets try DFS
# Define the environment - use numpy and add in obstacles
# Define robot - contraints on its movements
# do DFS untill no more nodes left to explore
# Make sure to keep track of all waypoints

############################################################################
# Defining Workspace
def Maze(length, breadth, obstacles):
    # fn takes dimensions for maze
    # obstacles is a nested list [centre,diameter]
    # returns workspace - numpy matrix with 0s and 1s as elements
    maze = np.zeros((length+2,breadth+2))
    
    # making obstacles as 1s
    for i in range(length+2):
        for j in range(breadth+2):
            if ((j-5)**2+(i-(5))**2 <= 2**2): # circle
                maze[i][j] = 1

    # making borders to be as obstacles
    maze[0][:] = 1
    maze[11][:] = 1
    maze[:,11] = 1
    maze[:,0] = 1
    return maze

maze = Maze(10,10,1)
############################################################################
# Define Robot

# initial position of robot
p = [10,1]

def N(p):
    # check if no obs i.e 0 then move
    p_x, p_y = p[0], p[1]
    if maze[p_x-1][p_y] == 0:
        p_mod = [p_x-1,p_y]
        return p_mod
    else: return False
    
def S(p):
    # check if no obs i.e 0 then move
    p_x, p_y = p[0], p[1]
    if maze[p_x+1][p_y] == 0:
        p_mod = [p_x+1,p_y]
        return p_mod
    else: return False
    
def E(p):
    # check if no obs i.e 0 then move
    p_x, p_y = p[0], p[1]
    if maze[p_x][p_y+1] == 0:
        p_mod = [p_x,p_y+1]
        return p_mod
    else: return False
    
def W(p):
    # check if no obs i.e 0 then move
    p_x, p_y = p[0], p[1]
    if maze[p_x][p_y-1] == 0:
        p_mod = [p_x,p_y-1]
        return p_mod
    else: return False
############################################################################
# DFS
open_list = [p]
vis_list = [[-1,-1]]
while open_list != []:
    p = open_list[0] # first element of open list is current position
    vis_list.append(open_list.pop(0))
    maze[p[0],p[1]] = 1
    plt.imshow(maze), plt.show()
    temp = N(p)
    if temp: 
        if temp not in vis_list: open_list.insert(0,temp) # if north movement is possible then add that position to open list
    temp = E(p)
    if temp: 
        if temp not in vis_list: open_list.insert(0,temp)
    temp = S(p)
    if temp: 
        if temp not in vis_list: open_list.insert(0,temp)
    temp = W(p)
    if temp: 
        if temp not in vis_list: open_list.insert(0,temp)
    # vis_list.append(open_list.pop(0)) # pop current position and add to visited list
        
        
        







