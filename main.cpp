#include <iostream>
#include <vector>
#include <algorithm>
// printing maze /////////////////////////////////////////////////////////
// help functions here
void printMaze(int x, int y, int maze[12][12])
{
    for (y = 0; y < 12; ++y) {
        for (x = 0; x < 12; ++x)
            std::cout << maze[y][x] << "|";
        std::cout << std::endl;
    }
    std::cout << " " << std::endl;
}
// print waypoints
void printwaypoints(std::vector<std::vector<int>> vec){
    int i;
    int j;
    for (int i = 0; i < vec.size(); i++)
    {
        for (int j = 0; j < vec[i].size(); j++)
        {
            std::cout << vec[i][j] << ",";
        }
        std::cout << " ";
    }
}
//////////////////////////////////////////////////////////////////////////
// check if the directions is open or blocked
bool check_N(std::vector<int> p, int maze[12][12])
{if (maze[p[0]-1][p[1]] != -1) {return true;}
    else {return false;}}
bool check_S(std::vector<int> p, int maze[12][12])
{if (maze[p[0]+1][p[1]] != -1) {return true;}
    else {return false;}}
bool check_E(std::vector<int> p, int maze[12][12])
{if (maze[p[0]][p[1]+1] != -1) {return true;}
    else {return false;}}
bool check_W(std::vector<int> p, int maze[12][12])
{if (maze[p[0]][p[1]-1] != -1) {return true;}
    else {return false;}}
// move robot to a specific direction
int go_N(int px) {return px-1;}
int go_S(int px) {return px+1;}
int go_E(int py) {return py+1;}
int go_W(int py) {return py-1;}

// def stuck(curr_p):
//    # if there is no 0 around
//    if not check_N(curr_p) and not check_S(curr_p) and not check_E(curr_p) and not check_W(curr_p):
//        return True
//    else: return False //

// check wether robot is stuck and has to backtrack
bool stuck(std::vector<int> p, int maze[12][12]){
    if(check_N(p,maze) == 0 && check_S(p,maze) == 0 && check_E(p,maze) == 0 && check_W(p,maze) == 0){
        return true;}
    else{return false;}
}
// def move(p,maze):
//    while not stuck(p):
//        if check_N(p):
//            p = go_N(p)
//            maze[p[0]][p[1]] = -1
//        elif check_E(p):
//            p = go_E(p)
//            maze[p[0]][p[1]] = -1
//        elif check_S(p):
//            p = go_S(p)
//            maze[p[0]][p[1]] = -1
//        elif check_W(p):
//            p = go_W(p)
//            maze[p[0]][p[1]] = -1
//
//        waypoints.append(p)
//
//        plt.imshow(maze),plt.show()
//    return p,maze //

// struct keeps track of position and waypoints
struct ret {
    std::vector<int> p;
    int maze[12][12];
    std::vector<std::vector<int>> waypoints;
};
// move like bug till stuck
ret move(std::vector<int> p, int maze[12][12], std::vector<std::vector<int>> waypoints){
    ret n;
    n.p = p;
    n.maze[12][12] = maze[12][12];
    n.waypoints = waypoints;

    while (stuck(p, maze)==0){
        if (check_N(p,maze) == 1){
            p[0] = go_N(p[0]);
            maze[p[0]][p[1]] = -1;
        }
        else if (check_E(p,maze) == 1){
            p[1] = go_E(p[1]);
            maze[p[0]][p[1]] = -1;
        }
        else if (check_S(p,maze) == 1){
            p[0] = go_S(p[0]);
            maze[p[0]][p[1]] = -1;
        }
        else if (check_W(p,maze) == 1){
            p[1] = go_W(p[1]);
            maze[p[0]][p[1]] = -1;
        }
        n.waypoints.push_back({p[0],p[1]});
        n.p = {p[0],p[1]};
        printMaze(12,12,maze);

    }
    return n;
}

// def backtrack():
//    for p in waypoints[::-1]:
//        if stuck(p):
//            waypoints.append(p)
//            continue
//        else:
//            waypoints.append(p)
//            return True
//            # p,maze = move(p,maze)
//    return False //

//bool backtrack(){
  //  for (p)
//}

// chcek if all cells are explored
bool check_bt(std::vector<std::vector<int>> vec, int maze[12][12]){
    std::vector<int> p;
    std::vector<std::vector<int>> temp;
    for(auto& row:vec){
            //do something using the element col
            p = row;
            temp.push_back(p);
    }
    std::reverse(temp.begin(), temp.end());
    for(auto& row:temp){
        p = row;
        if (stuck(p,maze) == 1){
            vec.push_back(p);
            continue;}
        else {
            vec.push_back(p);
            return true;
        }
    }return false;}
// get position to backtrack to, get new valid position
std::vector<int> bt(std::vector<std::vector<int>> vec, int maze[12][12]){
    std::vector<int> p;
    std::vector<std::vector<int>> temp;
    for(auto& row:vec){
        //do something using the element col
        p = row;
        temp.push_back(p);
    }
    std::reverse(temp.begin(), temp.end());
    for(auto& row:temp){
        p = row;
        if (stuck(p,maze) == 1){
            vec.push_back(p);
            continue;}
        else {
            vec.push_back(p);
            return p;
        }
}
}

int main() {
/////////////////////////////////////////////////////////////////////
// define work space
    int maze[12][12] = {0};
    int i;
    int j;
    for (i = 0; i < 12; i++) {
        for (j = 0; j < 12; j++) {
            maze[0][j] = -1;
            maze[11][j] = -1;
            if (((j - 5) * (j - 5) + (i - 5) * (i - 5) <= 2 * 2)) {
                maze[i][j] = -1;
            }
            maze[i][11] = -1;
            maze[i][0] = -1;
        }
    }
//////////////////////////////////////////////////////////////////////////
    std::vector<int> p = {10, 1};
    std::vector<std::vector<int>> waypoints;

    // p, maze = move(p, maze)
    //while backtrack():
    //    p, maze = move(p, maze)
    //    backtrack()
    //    p = waypoints[-1]

    // main function

    ret mov = move(p, maze, waypoints);
    while (check_bt(mov.waypoints, maze) == 1){
        p = bt(mov.waypoints, maze);
        mov = move(p,maze,mov.waypoints);
    }
    printwaypoints(mov.waypoints);
}
